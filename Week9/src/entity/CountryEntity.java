package entity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "country", schema = "world", catalog = "")
public class CountryEntity {
    @OneToMany(mappedBy = "countryByCountryCode")
    private Collection<CityEntity> citiesByCode;
    @OneToMany(mappedBy = "countryByCountryCode")
    private Collection<CountrylanguageEntity> countrylanguagesByCode;

    public Collection<CityEntity> getCitiesByCode() {
        return citiesByCode;
    }

    public void setCitiesByCode(Collection<CityEntity> citiesByCode) {
        this.citiesByCode = citiesByCode;
    }

    public Collection<CountrylanguageEntity> getCountrylanguagesByCode() {
        return countrylanguagesByCode;
    }

    public void setCountrylanguagesByCode(Collection<CountrylanguageEntity> countrylanguagesByCode) {
        this.countrylanguagesByCode = countrylanguagesByCode;
    }
}
