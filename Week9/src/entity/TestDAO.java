/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package entity;

        import org.hibernate.Session;
        import org.hibernate.SessionFactory;
        import java.util.*;

/** TestDAO implemented using a singleton pattern
 *  Used to get city data from my MYSQL database*/
public class TestDAO {

    SessionFactory factory = null;
    Session session = null;

    private static TestDAO single_instance = null;

    private TestDAO()
    {
        factory = HibernateUtils.getSessionFactory();
    }

    /** This is what makes this class a singleton.  You use this
     *  class to get an instance of the class. */
    public static TestDAO getInstance()
    {
        if (single_instance == null) {
            single_instance = new TestDAO();
        }

        return single_instance;
    }

    /** Used to get more than one city from database
     *  Uses the OpenSession construct rather than the
     *  getCurrentSession method so that I control the
     *  session.  Need to close the session myself in finally.*/
    public List<world> getworld() {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.world";
            List<world> cs = (List<world>)session.createQuery(sql).getResultList();
            session.getTransaction().commit();
            return cs;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }

    }

    /** Used to get a single city from database */
    public world getworld(int id) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            String sql = "from entity.world where id=" + Integer.toString(id);
            world w = (world) session.createQuery(sql).getSingleResult();
            session.getTransaction().commit();
            return w;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

    public world insertCity(world w) {

        try {
            session = factory.openSession();
            session.getTransaction().begin();
            session.save(w);
            session.getTransaction().commit();
            return w;

        } catch (Exception e) {
            e.printStackTrace();
            // Rollback in case of an error occurred.
            session.getTransaction().rollback();
            return null;
        } finally {
            session.close();
        }
    }

}
