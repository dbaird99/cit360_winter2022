
/* This program is a part of a simple hibernate example used for CIT-360
   It is written by Troy Tuckett.  BYUI.EDU
 */
package entity;

import java.util.List;

public class RunHibernateExample {

    public static void main(String[] args) {

        TestDAO t = TestDAO.getInstance();

        List<world> c = t.getworld();
        for (world i : c) {
            System.out.println(i);
        }

        System.out.println(t.getworld(1));

        world w = new world();
        w.setName("Salt Lake City");
        w.setPopulation("197756");
        w.setDistrict("Utah");
        t.insertCity(w);

        System.out.println(t.getworld(4080));
    }


}