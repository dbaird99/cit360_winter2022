package entity;

import javax.persistence.*;

@Entity
@Table(name = "city")
public class world {
@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "Name")
    private String name;

    @Column(name = "Population")
    private String population;

    @Column(name = "District")
    private String district;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getDistrict() {return district;}

    public void setDistrict(String district) { this.district = district;}


    public String toString() {
        return Integer.toString(id) + " " + name + " " + population + " " + district;
    }
}

