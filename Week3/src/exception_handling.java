import java.util.Scanner;

public class exception_handling {

    public static void main(String[] args) {
        /* user input */
        Scanner userInput = new Scanner(System.in);
        float num1 = 11111;
        float num2 = 9999;
        String input;
        boolean checker = false;
        System.out.println("We're going to do some math. ");

        /* try block for the numerator */
        while(!checker) {

            try {
                System.out.print("Please enter a numerator: ");
                input = userInput.nextLine();
                num1 = Integer.parseInt(input);
                break;
            } catch (NumberFormatException iMe) {
                System.out.println("We're looking for whole numbers only. Please try again" );
            }
        }
        /* try block for the denominator */
        while(!checker) {

            try {
                System.out.print("Please enter a denominator: ");
                input = userInput.nextLine();
                num2 = Integer.parseInt(input);
                if(num2 == 0) {
                    System.out.println("A denominator cannot be zero. Please try again.");
                }
                else{
                    break;
                }

            } catch (NumberFormatException e) {
                System.out.println("Remember, whole numbers only. Try one more time.");
            }
        }

        float math = doDivision(num1, num2);
        /* do the math and display the result */
        System.out.println(num1 + " divided by " +num2 + " " + "equals " + math);
    }

    static float doDivision(float num1, float num2) throws ArithmeticException {
        return num1/num2;
    }
}