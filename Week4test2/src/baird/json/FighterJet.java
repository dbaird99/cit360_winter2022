//some code taken from Brother Tuckett demo video
package baird.json;

public class FighterJet {
    private String make;
    private String designator;
    private boolean stealth;
    private int cannon;

    public String getMake() {
        return make;
    }
    public void setMake(String make) {
        this.make = make;
    }
    public String getDesignator() {
        return designator;
    }
    public void setDesignator(String designator) {
        this.designator = designator;
    }
    public Boolean getStealth() {
        return stealth;
    }
    public void setStealth(Boolean stealth) {
        this.stealth = stealth;
    }
    public int getCannon() {
        return cannon;
    }
    public int setCannon(int cannon) {
        this.cannon = cannon;
        return cannon;
    }


    public String toString() {
        return "Make: " + make + ", Designator: " + designator + ", Stealth Capable: " + stealth + ", Cannon Caliber " + cannon + "mm";
    }
}
