//Worked with Marvel Okafor and Dainon Snow to figure out this code

package baird.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.net.*;
import java.io.*;
public class Client {

    //Code from Brother Tuckett.
    public static String getHttpContent(String string) {

        String content="";

        try {
            URL url = new URL(string);
            HttpURLConnection http = (HttpURLConnection) url.openConnection();

            BufferedReader reader = new BufferedReader(new InputStreamReader(http.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line= reader.readLine()) != null) {
                stringBuilder.append(line + "\n");
            }
            content = stringBuilder.toString();

        } catch (Exception e) {
            System.err.println(e.toString());
        }

        return content;
    }

    public static void main(String[] args) {

        System.out.println("\nIn JSON format: ");
        System.out.println(Client.getHttpContent("http://localhost:7200"));

        //Convert JSON to string in Jackson Library Pretty Printer Format. Code from kodejava.org.
        try {
            String json = (Client.getHttpContent("http://localhost:7200"));
            System.out.println("In Jackson Printer Pretty format: ");
            ObjectMapper mapper = new ObjectMapper();
            Object jsonObject = mapper.readValue(json, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
            System.out.println(prettyJson);

        }
        catch (Exception e){
            System.err.println(e.toString());
        }

        //From Brother Tuckett demo
        FighterJet jet2 = JSONtoFighterJet(Client.getHttpContent("http://localhost:7200"));
        System.out.println("\nIn Brother Tuckett Demo format: ");
        System.out.println(jet2);

        System.out.println("\n***The Program completed successfully.***");
    }

    //Method from Brother Tuckett
    public static FighterJet JSONtoFighterJet(String s) {

        ObjectMapper mapper = new ObjectMapper();
        FighterJet jet = null;

        try {
            jet = mapper.readValue(s, FighterJet.class);
        } catch (JsonProcessingException e) {
            System.err.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return jet;
    }
}
