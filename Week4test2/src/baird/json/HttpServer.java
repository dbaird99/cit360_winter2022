package baird.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;


public class HttpServer {

    public static void main(String[] args) throws IOException {
        com.sun.net.httpserver.HttpServer server = com.sun.net.httpserver.HttpServer.create(new InetSocketAddress(7200), 0);
        HttpContext context = server.createContext("/");
        context.setHandler(HttpServer::handle);
        server.start();
    }

    //Code from tabnine.com (how to use getResponseBody)
    private static void handle(HttpExchange exchange) throws IOException {
        String response = getData();
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        exchange.sendResponseHeaders(200, response.getBytes(StandardCharsets.UTF_8).length);
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }

    public static String getData() throws JsonProcessingException {

        FighterJet jet = new FighterJet();
        jet.setMake("General Dynamics");
        jet.setDesignator("F-16");
        jet.setStealth(false);
        jet.setCannon(20);

        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(jet);

        return json;
    }
}