public class Swimmers extends Thread implements Runnable {
    public Swimmers(String print) {
        super(print);
    }

    //counts the number of laps each swimmer swims until 6 are completed
    public void run() {
        for (int counter = 1; counter <= 6; counter++) {
            System.out.println(getName()  + "is on Lap " + counter);

            //randomly generated sleep time for each thread to simulate the swimmer's turn at the wall
            try {
                System.out.print(getName() + " is turning at the wall  |  ");
                Thread.sleep((int) (Math.random() * 1850));

            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }

        /* race results */
        System.out.println("\n" + getName() + "has finished the race." + "\n");
    }
}