import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SwimMeets {


    public static void main(String[] args) {

        ExecutorService poolSide = Executors.newFixedThreadPool(3);

        Swimmers swm1 = new Swimmers("Damien ");
        Swimmers swm2 = new Swimmers("Danica ");
        Swimmers swm3 = new Swimmers("Mila ");
        Swimmers swm4 = new Swimmers("Max ");
        Swimmers swm5 = new Swimmers("Molly ");

        poolSide.execute(swm1);
        poolSide.execute(swm2);
        poolSide.execute(swm3);
        poolSide.execute(swm4);
        poolSide.execute(swm5);

        poolSide.shutdown();


    }
}
