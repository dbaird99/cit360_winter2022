//Java Collections - Dustin Baird
package basic_collections;

import java.util.*;

public class collectionTypes {
    public static void main(String[] args) {

        System.out.println("-- List --");
        List rhyme = new ArrayList();
        rhyme.add("Eeny");
        rhyme.add("meeny");
        rhyme.add("miny");
        rhyme.add("moe.");
        rhyme.add("Catch");
        rhyme.add("A");
        rhyme.add("tiger");
        rhyme.add("by");
        rhyme.add("the");
        rhyme.add("toe.");

        boolean emptyTest = rhyme.isEmpty();
        if (emptyTest) {
            System.out.println("Error: List is empty");
            System.exit(0);
        } else
            for (Object str : rhyme) {
                System.out.println((String) str);

            }
        System.out.println("-- Queue --");
        Queue nextInLine = new PriorityQueue();
        nextInLine.add("We");
        nextInLine.add("are");
        nextInLine.add("are");
        nextInLine.add("happy");
        nextInLine.add("to");
        nextInLine.add("meet");
        nextInLine.add("you!");

        Iterator iterator = nextInLine.iterator();
        while (iterator.hasNext()) {
            System.out.println(nextInLine.poll());
        }

        System.out.println("-- TreeSet --");
        Set we = new TreeSet();
        we.add("We");
        we.add("are");
        we.add("are");
        we.add("happy");
        we.add("to");
        we.add("meet");
        we.add("you!");

        Iterator lists = we.iterator();
        while(lists.hasNext()) {
            Object element = lists.next();
            System.out.print(element + "\n");
        }

        System.out.println("-- List using Generics --");
        List<location> travelList = new LinkedList<location>();
        travelList.add(new location("Barcelona", "Spain"));
        travelList.add(new location("Paris", "France"));
        travelList.add(new location("Rome", "Italy"));
        travelList.add(new location("Budapest", "Hungary"));
        travelList.add(new location("Bucharest", "Romania"));
        travelList.add(new location("Moscow", "Russia"));
        for (location location : travelList) {
            System.out.println(location);
        }

        System.out.println("-- Set --");
        Set<String> littlePiggy = new HashSet<String>();
        littlePiggy.add("Market");
        littlePiggy.add("Home");
        littlePiggy.add("Roast Beef");
        littlePiggy.add("None");
        littlePiggy.add("Wee wee wee all the way home");

        if (littlePiggy.isEmpty()) {
            System.out.println("Set is empty");
        } else {
            System.out.println((littlePiggy));
        }
    }
}
