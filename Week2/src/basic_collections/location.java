package basic_collections;

public class location {
    private String city;
    private String country;

    public location(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public String toString() {
        return "City: " + city + " Country: " + country;
    }

}

