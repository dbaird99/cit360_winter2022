import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import static org.junit.jupiter.api.Assertions.*;

class JunitTest {

    //assertEquals
    @Test
    void getSportingEvent() {
        JUnit sport = new JUnit();

        sport.setSportingEvent("Basketball");

        try {
            assertEquals("Basketball", sport.getSportingEvent());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //assertTrue
    @Test
    void getPointsScored() {
        JUnit sport = new JUnit();
        sport.setPointsScored(20);

        try {
            assertTrue(sport.getPointsScored() > (5) && sport.getPointsScored() < (50));
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //assertNotNull
    @Test
    void getJerseyNumber() {
        JUnit player = new JUnit();
        player.setJerseyNumber(11);

        try {
            assertNotNull(player.getJerseyNumber());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //assertFalse
    @Test
    void getFoulsEarned() {
        JUnit player = new JUnit();
        player.setFoulsEarned(6);

        try {
            assertFalse(5 > player.getFoulsEarned());
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //assertSame
    @Test
    void getTeamName() {

        JUnit teamName = new JUnit();
        teamName.setTeamName("Jaguars");

        try {
            assertSame(teamName.getTeamName(), "Jaguars");
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    //assertNotSame
    @Test
    void getTeamColors() {
        {

            JUnit teamColors = new JUnit();
            teamColors.setTeamColors("Red");

            try {
                assertNotSame(teamColors.getTeamColors(), "Blue");
            } catch (Exception e) {
                System.err.println(e.toString());
            }
        }
    }

    //assertThat
    @Test
    void square() {
        JUnit math = new JUnit();
        int output = math.square(3);
        try {
            assertThat(27, output);
        } catch (AssertionFailedError e) {
            System.err.println("Try again");
            System.out.println();
        }
    }
    private void assertThat(int i, int output) {
    }

}




